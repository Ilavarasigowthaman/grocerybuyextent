package grocerybuy;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.relevantcodes.extentreports.ExtentTest;
import grocerybuy.GroceryUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.AfterMethod;


public class Extentgrocery extends GroceryUtils implements ITestListener, ISuiteListener {
    public static ExtentTest logger;
    public WebDriver driver;
    public static ExtentReports report;

    @Override
    public void onStart(ISuite suite) {
        //Create an html report for the suite that is executed
        report = new ExtentReports("./report/" + suite.getName() + "_Results.html");
        test = report.startTest("GroceryBuy");

    }

    @Override
    public void onFinish(ISuite suite) {
        report.flush();
    }

    @Override
    public void onTestStart(ITestResult result) {
        logger = report.startTest(result.getMethod().getMethodName());
        logger.log(LogStatus.INFO, "Executing test: " + result.getMethod().getMethodName());

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.log(LogStatus.INFO, "Finished executing test");
    }


    @Override
    public void onTestFailure(ITestResult result) {
        String fileName = String.format ( "Screenshot-%s.jpg", Calendar.getInstance ().getTimeInMillis () );
        WebDriver driver = (WebDriver) result.getTestContext ().getAttribute ( "WebDriver" ); //use string from setAttribute from BasePage
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs ( OutputType.FILE );
        File destFile = new File ( "./screenshots/" + fileName );
       // Reporter.log ("<br> <img src ='"+fileName+"' height= '400' width='400'/> <br/> ");
        if(result.getStatus () == ITestResult.FAILURE)
        {
            Reporter.log("This is failed log from reporter.log", false);
            test.log(LogStatus.PASS, "Navigated to the specified URL");
        }
        else
        {
            Reporter.log("This is failed log from reporter.log", true);
            test.log(LogStatus.FAIL, "Test Failed");
        }

            try {
                FileUtils.copyFile ( srcFile, destFile );
                System.out.println ( "Screenshot taken, saved in screenshots folder" );
                Reporter.log ("<br> <img src ='"+fileName+"' height= '400' width='400'/> <br/> ");
                Reporter.log ( "<a href='" + destFile.getAbsolutePath () + "'> <img src='" + destFile.getAbsolutePath () + "' height='100' width='100'/> </a>" );
                test.log(LogStatus.FAIL,test.addScreenCapture(fileName)+ "Test Failed");
            } catch (IOException e) {
                System.out.println ( "Failed to take screenshot" );
            }

            logger.log ( LogStatus.FAIL, "Test failed, +./report/" + "_Results.html" );
        }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.log(LogStatus.SKIP, "Test skipped");
    }}
