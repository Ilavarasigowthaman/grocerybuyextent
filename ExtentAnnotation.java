package grocerybuy;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

import org.testng.annotations.AfterMethod;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.BeforeTest;


import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentAnnotation {

        ExtentReports extent;
        ExtentTest test;
        WebDriver driver;

        @BeforeTest
        public void init()
        {
            extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/ExtentScreenshot.html", true);
        }

        @Test
        public void captureScreenshot()
        {
            test = extent.startTest("captureScreenshot");
            driver = new FirefoxDriver();
            driver.get("http://automationtesting.in");
            String title = driver.getTitle();
            Assert.assertEquals("Home - Automation Test", title);
            test.log(LogStatus.PASS, "Test Passed");
        }

        @AfterMethod
        public void getResult(ITestResult result) throws IOException {
            if (result.getStatus () == ITestResult.FAILURE) {

                String screenShotPath = GetScreenShot.capture(driver, "screenShotName");

                test.log ( LogStatus.FAIL, result.getThrowable () );
                test.log ( LogStatus.FAIL, "Snapshot below: " + test.addScreenCapture ( screenShotPath ) );
            }
        }


        @AfterTest
        public void endreport()
        {
            driver.close();
            extent.flush();
            extent.close();
        }
    public class GetScreenShot {

        public static String capture(WebDriver driver,String screenShotName) throws IOException
        {
            TakesScreenshot ts = (TakesScreenshot)driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            String dest = System.getProperty("user.dir") +"\\ErrorScreenshots\\"+screenShotName+".png";
            File destination = new File(dest);
            FileUtils.copyFile(source, destination);

            return dest;
        }
    }
    }