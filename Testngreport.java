package grocerybuy;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import org.testng.annotations.Test;
import org.testng.internal.annotations.IListeners;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Testngreport extends Testngreportmain implements ITestListener, ISuiteListener {


    public WebDriver driver;
    static ExtentTest test;

@Override

    public void onTestFailure(ITestResult result) {
        String fileName = String.format ( "Screenshot-%s.jpg", Calendar.getInstance ().getTimeInMillis () );
        WebDriver driver = (WebDriver) result.getTestContext ().getAttribute ( "WebDriver" ); //use string from setAttribute from BasePage


        if(result.getStatus () == ITestResult.FAILURE) {

            File srcFile = ((TakesScreenshot) driver).getScreenshotAs ( OutputType.FILE );


            try {
                File destFile = new File ( "src/../Images/" + System.currentTimeMillis () + ".png" );
                FileUtils.copyFile ( srcFile, destFile );
                System.out.println ( "Screenshot taken, saved in screenshots folder" );
                Reporter.log ( "<br> <img src ='" + fileName + "' height= '400' width='400'/> <br/> " );
                Reporter.log ( "<a href='" + destFile.getAbsolutePath () + "'> <img src='" + destFile.getAbsolutePath () + "' height='100' width='100'/> </a>" );

            } catch (IOException e) {
                System.out.println ( "Failed to take screenshot" );
            }

        }}}